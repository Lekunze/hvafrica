// import { document } from "../../functions/node_modules/firebase-functions/lib/providers/firestore";
//const nodemailer = require('nodemailer');

// Initialize Firebase
var config = {
    apiKey: "AIzaSyDcIN6nonr3kT_Hxpt0LNDHHwSaifKSvYY",
    authDomain: "hvafrica-85e6e.firebaseapp.com",
    databaseURL: "https://hvafrica-85e6e.firebaseio.com",
    projectId: "hvafrica-85e6e",
    storageBucket: "hvafrica-85e6e.appspot.com",
    messagingSenderId: "142475971858"
  };
firebase.initializeApp(config);
    

    /**
     * Handle user logout
     */
    function logout(){
        firebase.auth().signOut().then(function() {
            //location.reload();
            window.location = '/login';                        
            console.log("Signout successful");
          }).catch(function(error) {
            // An error happened.
            console.log("Logout-error:" + error);
        });

    }


    /**
     * Callback function to login
     */
    function loginUser(){
        window.location = '/welcome.html';                
    }
    

    
    /**
     * Sign in with Facebook Account
     */
    function signInWithFacebook(){
        if (!firebase.auth().currentUser) {
          var provider = new firebase.auth.FacebookAuthProvider();
          //provider.addScope('user_birthday');

          firebase.auth().signInWithPopup(provider).then(function(result, login) {

              var token = result.credential.accessToken;
              var user = result.user;
              //console.log('To String: ' + toString(user));
              //console.log('Stringify: ' + JSON.stringify(result));
              //console.log('Profile: ' + result.additionalUserInfo.profile.link);
            //let promise = newFBUser(result, user);            
            //promise.then(loginUser());
            //$("#exampleModal").modal('show');            
            loginUser();
          }).catch(function(error) {
            // Handle Errors here.

            $("#exampleModalLabel").html("New User Error");
            $("#exampleModalBody").html(error.message);
            $("#exampleModal").modal('show')

            var errorCode = error.code;
            //var erdororMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // [START_EXCLUDE]
            if (errorCode === 'auth/account-exists-with-different-credential') {
              //alert('You have already signed up with a different auth provider for that email.');
              // If you are using multiple auth providers on your app you should handle linking
              // the user's accounts here.
            } else {
              //console.error(error);
            }
            // [END_EXCLUDE]
          });
          // [END signin]
          } else {
            logout();
        }
    }

    /**
     * Sign in with google account
     */
    // [START buttoncallback]
    function signInWithGoogle() {
        if (!firebase.auth().currentUser) {
          // [START createprovider]
          var provider = new firebase.auth.GoogleAuthProvider();
          // [END createprovider]
          // [START addscopes]
          provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
          // [END addscopes]
          // [START signin]
          firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            // [START_EXCLUDE]
            //document.getElementById('quickstart-oauthtoken').textContent = token;
            // [END_EXCLUDE]
            //$("#exampleModal").modal('show');                        
            loginUser();
          }).catch(function(error) {

            $("#exampleModalLabel").html("New User Error");
            $("#exampleModalBody").html(error.message);
            $("#exampleModal").modal('show')

            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // [START_EXCLUDE]
            if (errorCode === 'auth/account-exists-with-different-credential') {
              //alert('You have already signed up with a different auth provider for that email.');
              // If you are using multiple auth providers on your app you should handle linking
              // the user's accounts here.
            } else {
              console.error(error);
            }
            // [END_EXCLUDE]
          });
          // [END signin]
        } else {
          // [START signout]
          firebase.auth().signOut();
          // [END signout]
        }
        // [START_EXCLUDE]
        //document.getElementById('quickstart-sign-in').disabled = true;
        // [END_EXCLUDE]
      }
      // [END buttoncallback]

    
    
    /**
     * Login with email and password
     */ 
    function signInWithEmailAndPassword(){
        if (firebase.auth().currentUser) {
            logout();
        }
            
        //Initialize form variables
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        
        firebase.auth().signInWithEmailAndPassword(email, password).then(function(snap){
            window.location = "/welcome.html";
        }).catch(function(error) {
            // Handle Errors here.
            $("#loginModalBody").html(error.message);
            $("#loginModal").modal('show')
            
        });
        
    }


    /**
     * Create user with email and password
     */
    function createUserWithEmailAndPassword(){
        //Initialize form variables
        var email = document.getElementById('email-new').value;
        var name = document.getElementById('name-new').value;
        var password = document.getElementById('password-new').value;
    

        var promise = firebase.auth().createUserWithEmailAndPassword(email, password);
        promise.then(function(user){
            if(user){                          
                firebase.auth().currentUser.updateProfile({
                    displayName: name
                });
            }
            $("#exampleModal").modal('show');
            
        }, function(error){
            if(error.code == 'auth/email-already-in-use'){
                document.getElementById("acc-err-msg").innerHTML = "User already exists. Create new user or Login";                
            }
            if(error.code == 'auth/weak-password'){
                document.getElementById("acc-err-msg").innerHTML = "We want your account to be secure. Please try a stronger password.";                                
            }

            $("#exampleModalLabel").html("New User Error");
            $("#exampleModalBody").html(error.message);
            $("#exampleModal").modal('show')
            
        });
        
    }

   
   
    function initApp(){
        //Listen for auth state changes
        firebase.auth().onAuthStateChanged(function(user){
            if(user){
                $("#user-actif").html('<i class="fa fa-user"></i>');
                $("#user-actif").attr("href","");                
                $("#user-actif").attr("target","");
                $("#user-actif").on('click', function(){
                    logout();
                })
            }else{
                $("#user-actif").html('<i class="fa fa-user-o"></i>');
                $("#user-actif").attr("href","/login");
            }
        });

        //Add event listener to buttons, link to functions 
        

    }

    window.onload = function(){
        initApp();
    };

// }());