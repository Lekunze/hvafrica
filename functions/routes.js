'use strict';

const express = require('express');
const router = express.Router();
const path = require('path');
const admin = require('firebase-admin');
//const cookieParser = require('cookie-parser');
//const gcm = require('node-gcm');
//const nhbs = require('nodemailer-express-handlebars');


admin.initializeApp({
    credential: admin.credential.cert({
        projectId: 'keteke-90701',
        clientEmail: 'firebase-adminsdk-xatnn@keteke-90701.iam.gserviceaccount.com',
        privateKey: '-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDYYQxqmiDsAESL\ni5RqlGeY4R73zQI9BejCklQTFK3cg5gfItIKYOziG/AwRVqzORWxWDT41bfZ9XYh\n8UrTyxlsVWThT40lmjR+cvnTAUHb2qlTklJ1MLeLP0iN3uyZa6pj1O7jOzcCtbz3\nb3R1cgNCqHkpTei0UNXWHNXdUM4pYMuRvQjGOcc6XoL9EKDcy+Tlv2weTJhKUEcy\n+CLCw1nkMehMP3kDkG10IA4ish9CPyk8dqYElr0zJTunqLLtX2WkSNviKLCbBvNs\n9EmdT/ALY0Pv0O8/5aze2mTjyEFDZeNGSVfocW3nn8l6zW03uUMH7xy9uIqt/bYp\nKCJaLy5PAgMBAAECggEASfA7uG+rgUmEIrUTBappjh2bIe0dbzHGPDqZXR3LXVdg\nU39oC6DcQVVvqWcBm82NYpj0HPWWyfVtJSMZQS8dJF43xyASaL99YZozUQ2UbiaV\nhtewspfvmDcLOWNsMO7P/iYkbWRuI1Br3pdo0bsgbxY3tMwJERiwQWaAFAzJLezi\n7lnI8VUVwFSIYiB7sswrYsqOjlN1GrQncVHkYbxFhPky2EjRm1TYqGTkTpCN1r/I\nzZh4sE+L6BeWkpsON4VzOTGvsfzmkBrr6JDSlot1zBlWyqN+e8oyisfHeFXnFDwQ\ng8I1o4cJuURPqU+GbmdVD3xZc15A/n8tLWaHZTlklQKBgQD6BttkotLaNCumnRXl\nWVtnZ82yGNTGwGpxaSOUvMQQ0/CQPY97K80YH+zxFT/vrsYzSbVjchVmQWOExFkF\n066FtdYCneQphf13S22GbQRECrpM+npYJf4k3BZfPSeQQQT0i4tusIIHanqPBpPx\nj1gHyYhcgSU7PzasGM82GJJbAwKBgQDdjGewGBuHfd7a28KJrkwSzalfnKdREiyM\nRgRXYrz8eHA29rbIdlho+JIlXRcM32X3hvQx7MsUuf0gikyhuapZr3Z3znRuJnhX\nLGgM4HhAFvYJSfBIognj5X9p7myGe6WTOqC431trFK3rfBHu9Jb85itXwFez86GK\n2scOG4C3xQKBgQDOyIEG2Es+wEshv2Vui9q2yPksRLElt9nQ9j3BM+WMbVzWTrOu\nEOnM/AqA0SxR1OCDZoCoQKBGM+pU0fc2hH4gg+T9wQwE85ZP5Ygt11fh1mWCaN5q\nrPFmFshB+FgSTFQG1Kp632yLKZs3Ux9kd0M9HYazyvO57mwPJW2qtePFdQKBgQDA\nKx9tQRvv5KA/WktdvU/DJ5z4kBsZWRhG/Byb58thet5q7okRTIXXV3T878gkpEn/\n8JE0FN76NXF4nj1mMnOuo2Gw/eVD/h54Ds6aDMKBZOk4mWfQuKy3fsOMEQEVjX7L\nPBlZAnR7Z06MaevsR0AIUWXbEb2BG5xNowLyG60EuQKBgQCsH1NyynUmU84dPVEL\nP1iSAmpcRuACUzHxJb3dvfGNC9XGXASwyuSxGrN9FX+jfk/6C1v0qpWNiq41PA1N\n2gw4b7OLNzNu4StwSn1XqkKuOfXgLUfuo8jrc2eOmq9VJBazIWGjKQT8x/RucvDE\nADziCsNlbYeV7iCr6Kv+Fcr6Eg==\n-----END PRIVATE KEY-----\n'
    }),
  databaseURL: 'https://keteke-90701.firebaseio.com'
});

const database = admin.database();


//router.get('/login', function(request, response){
//    response.render('login-choice',{title: 'Login to your Account | Raba Rides'});
//});

router.get('/en-gh', function(request, response){
    response.render('home', {layout:false});    
});

router.get('/shipping-policy', function(request, response){
    response.render('shipping-policy', {title: 'Shipping Policy', layout: 'policies'});    
});

router.get('/privacy-policy', function(request, response){
    response.render('privacy-policy', {title: 'Privacy Policy', layout: 'policies'});    
});

router.get('/payment-policy', function(request, response){
    response.render('payment-policy', {title: 'Payment Policy', layout: 'policies'});    
});

router.get('/terms-and-conditions', function(request, response){
    response.render('terms', {title: 'Terms and Conditions', layout: 'policies'});    
});

router.get('/about', function(request, response){
    response.render('about', {title: 'About Us', layout: 'policies'});    
});

router.get('/contact', function(request, response){
    response.render('contact', {title: 'Contact Us', layout: 'policies'});    
});

router.get('/faqs', function(request, response){
    response.render('faq', {title: 'Frequently Asked Questions', layout: 'policies'});    
});

router.get('/delivery-exchanges', function(request, response){
    response.render('delivery-exchanges', {title: 'Delivery & Exchanges', layout: 'policies'});    
});

router.get('/order-status', function(request, response){
    response.render('order-status', {title: 'Order Status', layout: 'policies'});    
});


module.exports = router;


