const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const hbs = require('express-handlebars');
const path = require('path');

const router = require('./routes');
const app = express();
//var routerX = express.Router();

// handlebars setup
app.engine('hbs', hbs({extname:'hbs', defaultLayout:'categories', layoutsDir:__dirname+'/views/layouts',
partialsDir: __dirname + '/views/components/'}));
app.set('views', path.join(__dirname,'views'));
app.set('view engine', 'hbs');
app.use(express.static(path.join(__dirname,'public')));

//adding event listener for booking rides
// send ride alert on ride creation

app.use(router);




exports.config = functions.https.onRequest(app);
